Source: dialog
Section: misc
Priority: optional
Maintainer: Santiago Vila <sanvila@debian.org>
Standards-Version: 4.6.2
Build-Depends: debhelper-compat (= 13), gettext, libncurses-dev (>= 5.3)
Homepage: https://invisible-island.net/dialog/dialog.html
Rules-Requires-Root: no

Package: dialog
Architecture: any
Depends: debianutils (>= 1.6), ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: foreign
Description: Displays user-friendly dialog boxes from shell scripts
 This application provides a method of displaying several different types
 of dialog boxes from shell scripts.  This allows a developer of a script
 to interact with the user in a much friendlier manner.
 .
 The following types of boxes are at your disposal:
  yes/no           Typical query style box with "Yes" and "No" answer buttons
  menu             A scrolling list of menu choices with single entry selection
  input            Query style box with text entry field
  message          Similar to the yes/no box, but with only an "Ok" button
  text             A scrollable text box that works like a simple file viewer
  info             A message display that allows asynchronous script execution
  checklist        Similar to the menu box, but allowing multiple selections
  radiolist        Checklist style box allowing single selections
  gauge            Typical "progress report" style box
  tail             Allows viewing the end of files (tail) that auto updates
  background tail  Similar to tail but runs in the background.
  editbox          Allows editing an existing file

Package: libdialog15
Architecture: any
Section: libs
Depends: ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Description: Displays user-friendly dialog boxes -- shared library
 The dialog application provides a method of displaying several different
 types of dialog boxes from shell scripts.  This allows a developer of a
 script to interact with the user in a much friendlier manner.
 .
 This package contains the shared library.

Package: libdialog-dev
Architecture: any
Section: libdevel
Depends: ${misc:Depends}, libdialog15 (= ${binary:Version}), libncurses-dev
Multi-Arch: same
Replaces: dialog (<< 1.3-20240307-1~)
Breaks: dialog (<< 1.3-20240307-1~)
Description: Displays user-friendly dialog boxes -- development files
 The dialog application provides a method of displaying several different
 types of dialog boxes from shell scripts.  This allows a developer of a
 script to interact with the user in a much friendlier manner.
 .
 This package contains the development files and the API documentation.
